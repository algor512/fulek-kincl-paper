\documentclass[a4paper,12pt,oneside]{article}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{hyperref}
\usepackage[capitalise,nameinlink]{cleveref}
\usepackage[dvipsnames]{xcolor}
\usepackage{indentfirst}
\usepackage{enumitem}
\usepackage{titling}
\usepackage{todonotes}
\usepackage{float}

\definecolor{blue}{RGB}{34,113,178}
\definecolor{pink}{RGB}{247,72,165}

\hypersetup{
  pagebackref,
  colorlinks,
  linkcolor=blue,
  citecolor=pink,
  urlcolor=pink
}

\theoremstyle{plain}
\newtheorem{prop}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{falselemma}[lemma]{False Lemma}

\theoremstyle{definition}
\newtheorem*{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem*{remark}{Remark}
\newtheorem*{conjecture}{Conjecture}

\setlength{\parindent}{0cm}
\setlength{\parskip}{0.5em}

\setlist{listparindent=\parindent,parsep=\parskip}

\newcommand{\hl}[1]{\textcolor{red}{#1}}

\newcommand{\from}{\colon}
\newcommand{\xto}[1]{\xrightarrow{#1}}
\newcommand{\wt}[1]{\widetilde{#1}}

\newcommand{\id}{\mathrm{id}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\HH}{\mathcal{H}}
\newcommand{\JJ}{\mathcal{J}}
\newcommand{\WW}{\mathcal{W}}
\newcommand{\Conf}{\mathrm{Conf}}
\newcommand{\Mod}[1]{\ (\mathrm{mod}\ #1)}
\DeclareEmphSequence{\bfseries,\itshape,\upshape}

\DeclareMathOperator{\pr}{pr}
\DeclareMathOperator{\sk}{sk}
\DeclareMathOperator{\st}{st}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Int}{Int}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\begin{document}

Let $H$ be a graph embedded into a surface, $\HH$ be the corresponding ribbon graph. Suppose we have
a graph homomorphism $f \from G \to H$ and its $\Z_{2}$-approximation $\psi \from G \to \HH$.  We
denote vertices and egdes of $\HH$ corresponding to $v \in V(H)$ and $vw \in E(H)$, respectively, by
$D_{v}$ and $D_{vw}$.

Moreover, suppose $f$ is locally injective and every connected component of $\psi^{-1}(D_{v})$ is a
star.

Let $G = R \sqcup P$ where $P$ is a path graph. Assume $\psi \big|_{R}$ is an embedding. We plan to
answer the following question: is it possible to find an approximation $\phi' \from G \to \HH$
coinciding with $\phi$ on $R$?

Every path $p \from J \to H$ induces a map $p' \from \JJ \to \HH$, where $\JJ$ is the ribbon graph
of a path graph $J$, which is a homomorphism on each vertex and on each edge of $\JJ$. We say that
\hl{orientations of vertices $D_{v}$ and $D_{w}$ of $\HH$ are compatible with respect to a path
$p \from J \to H$ from $v$ to $w$} if we can choose an orientation of $\JJ$ such that the restriction
of $p'$ to the endpoints of $\JJ$ gives us orientation-preserving homeomorphisms with $D_{v}$ and
$D_{w}$, respectively.

Given an orientation on $D_{v}$, we have a cyclic order on the points of $\partial D_{v}$, and hence
on the set of valves $\WW_{v} = \left\{ W_{vw} = D_{v} \cap D_{vw} \subset \partial D_{v} \right\}$.

\hl{A part of $G$ in $D_{v}$} is a connected component of $\psi^{-1}(D_{v})$ containing a vertex of
degree more than one. We say that a part $G_{v}$ of $G$ in $D_{v}$ is incident to a valve $W_{vw}$
if it intersects $W_{vw}$ (obviously, it must intersect it in a single point). By $\WW_{v}(G_{v})$
we denote the set of valves incident to $G_{v}$.

\begin{lemma}[Partial cyclic orders on parts of $G$]
  Let $D_{v}$ be a vertex of $\HH$. Fix an orientation of $D_{v}$ and, thus, a cyclic order on
  $\WW_{v}$. Let $G_{W}$ be the set of parts of $G$ in $D_{v}$ incident to $W$, and let $\prec_{W}$
  be the order on $\WW_{v} \setminus \{ W \}$ induced by the cyclic order.

  For parts $G^{1}_{v}, G^{2}_{v} \in G_{W}$ define $G^{1}_{v} <_{W} G^{2}_{v}$ if there are
  valves $W^{1}$ and $W^{2}$, incident to $G^{1}_{v}$ and $G^{2}_{v}$, respectively, such that
  $W^{1} \succ_{W} W^{2}$.

  We claim that
  \begin{enumerate}[nosep]
  \item $<_{W}$ is well-defined partial order on $G_{W}$.
  \item Parts $G^{1}_{v}, G^{2}_{v}$ are not comparable if and only if they both are curves (with a
    single vertex of degree two on each) connecting the same pair of valves.
  \end{enumerate}
\end{lemma}

\begin{lemma}[Orders of different valves]\label{l:ord:valves}
  Let $G^{1}_{v}, G^{2}_{v}$ be parts of $G$ in $D_{v}$ sharing two valves $W_{1}$ and $W_{2}$.
  If $G^{1}_{v} <_{W_{1}} G^{2}_{v}$ then $G^{1}_{v} >_{W_{2}} G^{2}_{v}$.
\end{lemma}

We say that \hl{a pair $(G^{1}_{v}, G^{2}_{v})$ of parts of $G$ in $D_{v}$ is connected by a double path
with a pair $(G^{1}_{w}, G^{2}_{w})$ of pairs of $G$ in $D_{w}$} if there are two paths $p_{1} p_{2}
\dots p_{l}$ and $q_{1} q_{2} \dots q_{l}$ in $G$ such that
\begin{enumerate}
\item $\psi(p_{1}) \in G^{1}_{v}$, $\psi(q_{1}) \in G^{2}_{v}$, $\psi(p_{l}) \in G^{1}_{w}$,
  $\psi(q_{l}) \in G^{2}_{w}$,
\item $p_{i} \neq q_{i}$ and $f(p_{i}) = f(q_{i})$ for every $i$.
\end{enumerate}
\begin{lemma}[Double paths preserve orders]\label{l:ord:paths}
  Let $D_{v}$ and $D_{w}$ be vertices of $\HH$, $(G^{1}_{v}, G^{2}_{v})$ be a
  pair of parts of $G$ in $D_{v}$ connected by a double path
  $(p_{1}\dots{}p_{l}, q_{1}\dots{}q_{l})$ with a pair $(G^{1}_{w}, G^{2}_{w})$ of parts of $G$ in
  $D_{w}$.

  Choose compatible orientations of $D_{v}$ and $D_{w}$ with respect to the path
  $f(p_{1}) f(p_{2}) \dots f(p_{l})$ in $H$.

  If $G^{1}_{v} <_{W_{vf(p_{2})}} G^{2}_{v}$ then either $G^{1}_{w} >_{W_{wf(p_{l-1})}} G^{2}_{w}$
  or $G^{1}_{w}, G^{2}_{w}$ are not comparable.
\end{lemma}

\begin{lemma}[Orders define an embedding]\label{l:ext:emb}
  $f$ is approximated by embeddings if and only if the partial orders $<_{W_{vw}}$ can be extended
  to linear orders satisfying~\cref{l:ord:valves} and~\cref{l:ord:paths}.

  Moreover, if $G = R \sqcup R'$, and $\psi \big|_{R}$ is an embedding, we can extend $<_{W_{vw}}$
  so that any two parts of $R$ are comparable; to do so, we can just use the orders on the endpoints
  of these parts.
\end{lemma}

We can try to extend the partial orders induced by an $\Z_{2}$-approximation as follows.

Let $G^{1}_{v}$ and $G^{2}_{v}$ be two non-comparable curves in $D_{v}$. We can follow them in both
directions, while their continuations stay non-comparable. There are two options: either one of
these curves ends or we get a disc $D_{w}$ and two comparable parts of $G$ in $D_{w}$ connected by a
double path with $G^{1}_{v}$ and $G^{2}_{v}$.

Note that, if we found such comparable parts while following in both directions from $D_{v}$, the
orders we get must be compatible, as they are connected by a double path. Thus, we can use these
orders to compare $G^{1}_{v}$ and $G^{2}_{v}$ in $D_{v}$.

It is easy to see that we can run into a problem if our initial parts are local parts of $k$-winding
with $k > 2$. Specifically, at some point, after applying transitivity, we will get a relation
opposite to the one we already have.

However, if the statement of Fulek-Kin\v{c}l theorem is true, it must be the only case where we run
into a problem. This doesn't seem very easy to prove, we'll try to prove this for the map
$f \big|_{P} \from P \to H$.


\begin{lemma}
  Assume $f \from P \to H$ is a locally injective map from a path graph, and $\psi \from P \to \HH$ is
  a $\Z_{2}$-approximation of it.

  Then, we can extend the partial orders $<_{\bullet}$ to the linear orders $<^{*}_{\bullet}$ as
  in~\cref{l:ext:emb}.
\end{lemma}

\begin{proof}
  Let $P^{(2)}$ be a graph whose vertices are pairs of vertices of $P$ with the same image, and
  whose edges are pairs of edges with the same image.\footnote{Note that these edges must be
    independent since $f$ is locally injective.} In other words, we can say that the vertices of
  $P^{(2)}$ are pairs of parts of $P$, including degree one parts, and two pairs of parts are
  connected by an edge if they are connected by a double path of length one.

  Therefore, every path in $P^{(2)}$ is a double path and vice versa. It is easy to see that every
  vertex of $P^{(2)}$ has degree $\leq 2$. Moreover, $P^{(2)}$ doesn't have cycles; indeed,
  otherwise we can find two cycles in $P$. Thus, $P^{(2)}$ is a collection of segments. Note that
  each connected component of $P^{(2)}$ represents a maximal double path. Let's remove all isolated
  points from $P^{(2)}$

  \begin{remark}
    It is worth to note that pairs $(x, y)$ and $(y, x)$ must lie in different connected components,
    otherwise we also have a cycle in $P$.
  \end{remark}

  Let $J$ be a connected component of $P^{(2)}$, and let $(x, y)$ be its endpoint. Therefore, there
  is a single edge $(e, g)$ incident with $(x, y)$. There are two options:
  \begin{enumerate}[nosep]
  \item If one of the vertices $x$ or $y$ is a degree one vertex we say that $(x, y)$ is \hl{a dead
      end}.
  \item Otherwise both $x$ and $y$ have degree two, but the edges $e' \neq e$ and $g' \neq g$
    incident to them go in different directions. In this case, we say that $(x, y)$ is \hl{a turn}.
  \end{enumerate}

  Now we claim the following:
  \begin{enumerate}[nosep]
  \item Every pair of parts sharing a valve (that is, parts on which we must define
    $<^{*}_{\bullet}$) is represented by a vertex of $P^{(2)}$.
  \item The parts represented by turns are comparable.
  \item If a connected component $J$ has two turns as the endpoints, the orders in the corresponding
    pairs of parts are compatible (by~\cref{l:ord:paths}).
  \item If $(x, y)$ is an internal vertex of $J$, and $(a, b)$ is a turn in $J$, there is a unique
    orders in the pair $(x, y)$ compatible with the order in $(a, b)$.\footnote{Recall that $x$ and
      $y$ share two valves, so we need to define two orders on them.}
  \end{enumerate}

  Thus, if we have a connected component $J$ that has a turn as an endpoint, we can extend the order
  in this endpoint to the parts represented by the vertices of $J$. Let's denote the resulting
  binary relations by $\prec_{\bullet}$.

  The resulting binary relation is not defined for points lying in the connected components whose
  endpoints are dead ends. Let's say that these connected components are \hl{enclosed}.

  \begin{lemma}[transitivity on non-enclosed component]
    Let $W$ be a valve of a vertex $D_{v} \in V(\HH)$, and let $\alpha, \beta, \gamma$ be parts
    sharing $W$. Let also $(a, b)$, $(b, c)$ and $(a, c)$ be vertices corresponding to pairs
    $(\alpha, \beta)$, $(\beta, \gamma)$, and $(\alpha, \gamma)$.

    If none of these vertices lies on an enclosed component then $(\alpha \prec_{W} \beta) \land
    (\beta \prec_{W} \gamma) \rightarrow (\alpha \prec_{W} \gamma)$.
  \end{lemma}
  \begin{proof}[Sketch of proof]
    Consider the maximal triple path containg the parts $\alpha$, $\beta$ and $\gamma$. There is a
    maximal double path of the same length containing a pair of these parts, and following the
    triple path. By the assumtions, it must contain a turn as an endpoint. However, the turn is a
    turn of the other double paths, thus one of the parts is bigger or lower than the others.
  \end{proof}

   Therefore, we need to extend $\prec_{\bullet}$ to the enclosed components so that it remains
   transitive.

   Let $J_{1}$ and $J_{2}$ be enclosed components, and let $a$ and $b$ be the vertices of degree one
   of $P$. It can be seen that one of the endpoints of $J_{i}, i=1,2$ contains $a$, while the other
   one contains $b$. Assume $J_{1}$ connects $(a, x_{1})$ with $(y_{1}, b)$, and $J_{2}$ connects
   $(a, x_{2})$ with $(y_{2}, b)$. We claim that
   \begin{enumerate}
   \item if $J_{1}$ and $J_{2}$ have the same length then $J_{1} = J_{2}$ (up to involution),
   \item if $J_{1}$ is shorter than $J_{2}$ then $J_{2}$ contains paths of $J_{1}$ as subpaths: let
     $a \xto{\alpha} y_{1}$ and $x_{1} \xto{\beta} b$ be the paths of $J_{1}$. Then the paths of $J_{2}$ must
     look like
     \[
       \begin{aligned}
         a     &\xto{\alpha}& &y_{1}& &\xto{\gamma_{a}}& &s&     &\xto{\delta_{a}}& x_{2} \\
         x_{2} &\xto{\delta_{b}}& &t& &\xto{\gamma_{b}}& &x_{1}& &\xto{\beta}&     b \\
       \end{aligned}.
     \]

     Therefore, we have a linear order on the enclosed components (up to involution), denoted by $\sqsubset$.
   \end{enumerate}

   Next we consider double and triple components (defined below) up to permutations of paths forming
   them.

   Below I list some (possibly useful in the future) properties of triple components. By \hl{a
     triple component} I mean a maximal triple path, or, equivalently, a connected component of a
   graph of triples. Each pair of paths forming a triple path belongs to a double component; thus, a
   triple component corresponds to three double components.

   \begin{lemma}[Properties of triple components]
     Assume $T$ is a triple component. Then
     \begin{enumerate}[nosep]
     \item There is a double component corresponding to $T$ with the same length as $T$ (and going
       along $T$). We call this component \hl{a minimal component corresponding to $T$}. Note that
       there might be more than one minimal components.
     \item Let us define \hl{a cycle} with respect to some extension $\prec'_{\bullet}$ of
       $\prec_{\bullet}$ (by extension we mean just a relation, not necessarily an order) as a
       triple component realising oriented cycle in the directed graph induced by
       $\prec'_{\bullet}$.

       We claim that if $T$
       \begin{enumerate}[nosep]
       \item has at least one path whose both endpoints have degree more than one, or
       \item length of all the double paths corresponding to $T$ coincide, or
       \item there is a single minimal double component which is not enclosed\footnote{Just note
           that turns of this component induces turns in the other components.}
       \end{enumerate}
       then $T$ cannot be a cycle.
     \item If all three double components corresponding to $T$ have dead ends as the endpoints, the
       images of the vertices of $P$ form a periodic sequence, therefore, $f$ is a ``$k$-winding''
       where $k = \lfloor \frac{|P|}{p} \rfloor$ where $p$ is a period and $k \geq 2$.
     \end{enumerate}
   \end{lemma}
\end{proof}

\end{document}